const { Router } = require('express');

module.exports = () => {
    const router = Router();
    const movieList = [
        {id:1, title:'Glass', affiche:'5059914.jpeg',genres:'thriller fantastique', description:'Peu de temps après les événements relatés dans Split, David Dunn, l\'homme incassable, poursuit sa traque de La Bête, surnom donné à Kevin Crumb depuis qu\'on le sait capable d\'endosser 23 personnalités différentes. De son côté, le mystérieux homme souffrant du syndrome des os de verre Elijah Price suscite à nouveau l\'intérêt des forces de l\'ordre en affirmant détenir des informations capitales sur les deux hommes.',
            favorite:false, releaseDate:'16 janvier 2019', realization:'M. Night Shyamalan', actor:[
                "James McAvoy", "Bruce Willis", "Samuel L. Jackson", "Sarah Paulson", "Anya Taylor-Joy",]},
        {id:2, title:'DEATH WISH', affiche:'2800359.jpeg',genres:'action', description:'Quand il ne sauve pas des vies, Paul Kersey, chirurgien urgentiste, mène une vie de rêve, en famille, dans les beaux quartiers de Chicago. Jusqu\'au jour où tout bascule. Sa femme est sauvagement tuée lors d\'un cambriolage qui tourne mal. Sa fille de 18 ans est plongée dans le coma. Face à la lenteur de l\'enquête, il se lance dans une chasse à l\'homme sans merci.'
            , favorite:false, releaseDate:'2018', realization:'M.Eli Roth', actor:['Bruce Willis' ,'Vincent D\'Onofrio']},
        {id:3, title:'HARD KILL', affiche:'2275432.jpeg',genres:'action',description:'Peu de temps après les événements relatés dans Split, David Dunn, l\'homme incassable, poursuit sa traque de La Bête, surnom donné à Kevin Crumb depuis qu\'on le sait capable d\'endosser 23 personnalités différentes. De son côté, le mystérieux homme souffrant du syndrome des os de verre Elijah Price suscite à nouveau l\'intérêt des forces de l\'ordre en affirmant détenir des informations capitales sur les deux hommes.',
            favorite:false, releaseDate:'2020', realization:'M.Eli Roth', actor:['Bruce Willis', 'Jesse Metcalfe']}];
    favorites = [];

    router.get('/movies', async(req, res) => {
        res.status(200).json(movieList);
    })

    router.get('/movies/:id',async (req, res) => {
        res.status(200).json(movieList.filter(movie=> movie.id === parseInt(req.params.id))[0]);
    })

    router.post('/movies/:id/favorites',async (req, res) => {
        if(favorites.findIndex((element) => element.id === parseInt(req.params.id)) === -1){
            favorites.push(movieList.filter(movie=> movie.id === parseInt(req.params.id))[0]);
            movieList.forEach((element,index)=>{
                if(element.id === parseInt(req.params.id)){
                    movieList[index].favorite = !element.favorite;
                }
            })
        }
        res.status(200).json(favorites);
    })


    router.get('/favorites', async(req, res) => {
        res.status(200).json(favorites);
    })
    return router;
}
