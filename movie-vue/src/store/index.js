import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    movies:[],
    books:[],
    favoriteMovie:[]
  },
  getters:{

  },
  mutations: {
    setMovies(state, movies) {
      state.movies = movies;
    },
    addMoviesFavorite(state,id){
      state.favoriteMovie.push(state.movies.filter(movie=> movie.id === id ));
      state.movies.forEach((element,index)=>{
        if(element.id === id){
          state.movies[index].favorite = !element.favorite;
        }
      })
    },
    setFavorite(state, favorite) {
      state.favoriteMovie = favorite
    },
  },
  actions: {
    getMovies(context) {
      axios.get('http://localhost:2020/rest/movies')
          .then(result => {
            context.commit('setMovies', result.data)
          })
    },
    addMoviesFavorite(context,id){
      axios.post(`http://localhost:2020/rest/movies/${id}/favorites`)
          .then(result => {
            console.log(result);
            context.commit('addMoviesFavorite',id);
          })

    },
    getFavorite(context) {

      axios.get('http://localhost:2020/rest/favorites')
          .then(result => {
            context.commit('setFavorite', result.data)
          })

    },

  },
  modules: {

  }
})
