# vueJs-Nodejs-exemple

the prerequisites : 
- [Node.js](https://nodejs.org/fr/download/)
- [Vue CLI](https://cli.vuejs.org/)

You find attached the steps and commands to follow to deploy and start the project 

## Project movie-vue setup 
This project was generated with [Vue CLI](https://cli.vuejs.org/)

```
cd movie-vue
```
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```


## Project movie-vue setup 
```
cd server
```
```
npm install
```
```
npm run server
```
## Result

Navigate to `http://localhost:8080/`

